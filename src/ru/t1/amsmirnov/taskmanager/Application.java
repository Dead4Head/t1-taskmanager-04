package ru.t1.amsmirnov.taskmanager;

import static ru.t1.amsmirnov.taskmanager.constant.TerminalConst.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case HELP:
                showHelp();
                break;
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            default:
                showError();
        }
    }

    public static void showError() {
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported!");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Smirnov Anton");
        System.out.println("email: amsmirnov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info. \n", ABOUT);
        System.out.printf("%s - Show version info. \n", VERSION);
        System.out.printf("%s - Show command list. \n", HELP);
    }

}
